package message

import (
	"bytes"
	"encoding/binary"
)

type Association struct {
	EpochNano  int64
	StudentID  string
	PublicKey  string
}

func (a *Association) Serialize() (result []byte) {
	bint64 := make([]byte, 8)
	binary.LittleEndian.PutUint64(bint64, uint64(a.EpochNano))

	buffer := bytes.Buffer{}
	buffer.Write(bint64)
	buffer.WriteString("\uE000")
	buffer.WriteString(a.StudentID)
	buffer.WriteString("\uE000")
	buffer.WriteString(a.PublicKey)
	return buffer.Bytes()
}

func (a *Association) Deserialize(message []byte) {
	bs := bytes.Split(message, []byte("\uE000"))
	if len(bs) != 3 {
		panic("[Kafka] cannot deserialize message.")
	}
	a.EpochNano = int64(binary.LittleEndian.Uint64(bs[0]))
	a.StudentID = string(bs[1])
	a.PublicKey = string(bs[2])
}

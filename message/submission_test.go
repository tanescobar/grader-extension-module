package message

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSerializeAndDeserializeSubmission(t *testing.T) {
	s := Submission{
		EpochNano:  98765432123,
		StudentID:  "6131039421",
		PublicKey:  "PublicKey",
		SignedCode: "SignedCode",
	}

	s.Deserialize(s.Serialize())
	assert.Equal(t, Submission{
		EpochNano:  98765432123,
		StudentID:  "6131039421",
		PublicKey:  "PublicKey",
		SignedCode: "SignedCode",
	}, s)
}

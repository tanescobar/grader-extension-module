package kafka

import "github.com/Shopify/sarama"

func Consume(partition int32, c chan []byte) {
	partitionConsumer, err := consumer.ConsumePartition(topic, partition, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	for message := range partitionConsumer.Messages() {
		c <- message.Value
	}
}

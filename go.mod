module gitlab.com/wongtawan-j/grader-extension-module

go 1.15

require (
	github.com/Shopify/sarama v1.27.2
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/stretchr/testify v1.6.1
	golang.org/x/net v0.0.0-20201202161906-c7110b5ffcbb // indirect
)
